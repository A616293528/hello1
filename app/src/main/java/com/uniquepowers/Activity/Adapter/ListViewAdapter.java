package com.uniquepowers.Activity.Adapter;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.uniquepowers.Activity.Fragment.SingleItemView;
import com.uniquepowers.Activity.Fragment.SubCategory;
import com.uniquepowers.Activity.Model.SubcategoryModel;
import com.uniquepowers.R;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ListViewAdapter extends BaseAdapter {
	// Declare Variables
	Context mContext;
	LayoutInflater inflater;
	private List<SubcategoryModel> worldpopulationlist = null;
	private ArrayList<SubcategoryModel> arraylist;

	public ListViewAdapter(Context context, List<SubcategoryModel> worldpopulationlist) {
		mContext = context;
		this.worldpopulationlist = worldpopulationlist;
		inflater = LayoutInflater.from(mContext);
		this.arraylist = new ArrayList<SubcategoryModel>();
		this.arraylist.addAll(worldpopulationlist);
	}
	public class ViewHolder {
		TextView rank;
		TextView country;
		TextView population;
		TextView price;
	}
	@Override
	public int getCount() {
		return worldpopulationlist.size();
	}

	@Override
	public SubcategoryModel getItem(int position) {
		return worldpopulationlist.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	public View getView(final int position, View view, ViewGroup parent) {
		final ViewHolder holder;
		if (view == null) {
			holder = new ViewHolder();
			view = inflater.inflate(R.layout.grid_row, null);
			// Locate the TextViews in listview_item.xml
			holder.rank = (TextView) view.findViewById(R.id.name);
			holder.price = (TextView) view.findViewById(R.id.price);
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}
		// Set the results into TextViews
		holder.rank.setText(worldpopulationlist.get(position).getName());
		holder.price.setText("Price : $ "+worldpopulationlist.get(position).getPrice()+"");
		// Listen for ListView Item Click
//		view.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View arg0) {
//				// Send single item click data to SingleItemView Class
//				Intent intent = new Intent(mContext, SingleItemView.class);
//				// Pass all data rank
//				intent.putExtra("rank",(worldpopulationlist.get(position).getName()));
//				// Pass all data country
//
//				// Pass all data flag
//				// Start SingleItemView Class
//				mContext.startActivity(intent);
//
//
//
//			}
//		});
		return view;
}

	// Filter Class
	public void filter(String charText) {
		charText = charText.toLowerCase(Locale.getDefault());
		worldpopulationlist.clear();
		if (charText.length() == 0) {
			worldpopulationlist.addAll(arraylist);
		} 
		else 
		{
			for (SubcategoryModel wp : arraylist)
			{
				if (wp.getName().toLowerCase(Locale.getDefault()).contains(charText))
				{
					worldpopulationlist.add(wp);
				}
			}
		}
		notifyDataSetChanged();
	}
}
