package com.uniquepowers.Activity.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.uniquepowers.Activity.Model.CategoriesModel;
import com.uniquepowers.Activity.Model.OrderModel;
import com.uniquepowers.R;

import java.util.ArrayList;

public class OrderAdapter extends ArrayAdapter<OrderModel> implements View.OnClickListener{
private ArrayList<OrderModel> dataSet;
Context mContext;

// View lookup cache
private static class ViewHolder {
    TextView txtName;
    TextView id;
    TextView date;
    TextView txtVersion;
    ImageView info;
}

public OrderAdapter(ArrayList<OrderModel > data, Context context) {
    super(context, R.layout.row_item, data);
    this.dataSet = data;
    this.mContext=context;

}

@Override
public void onClick(View v) {

    int position=(Integer) v.getTag();
    Object object= getItem(position);
 OrderModel newsModel =(OrderModel)object;

    switch (v.getId())
    {
//            case R.id.item_info:
//                Snackbar.make(v, "Release date " + newsModel.getFeature(), Snackbar.LENGTH_LONG)
//                        .setAction("No action", null).show();
            //break;
    }
}

private int lastPosition = -1;

@Override
public View getView(int position, View convertView, ViewGroup parent) {
    // Get the data item for this position
   OrderModel newsModel = getItem(position);
    // Check if an existing view is being reused, otherwise inflate the view
    ViewHolder viewHolder; // view lookup cache stored in tag

    final View result;

    if (convertView == null) {

        viewHolder = new ViewHolder();
        LayoutInflater inflater = LayoutInflater.from(getContext());
        convertView = inflater.inflate(R.layout.row_item, parent, false);
        viewHolder.txtName = (TextView) convertView.findViewById(R.id.name);


        result=convertView;

        convertView.setTag(viewHolder);
    } else {
        viewHolder = (ViewHolder) convertView.getTag();
        result=convertView;
    }

//        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
//        result.startAnimation(animation);
      lastPosition = position;
      viewHolder.txtName.setText(newsModel.getName());

//
//        Picasso.with(getContext())
//                .load("http://agighana.org/uploaded_files/menu/"+newsModel.getFeature())
//                .into(viewHolder.info);

//        viewHolder.txtVersion.setText(newsModel.getVersion_number());
//        viewHolder.info.setOnClickListener(this);
//        viewHolder.info.setTag(position);
    // Return the completed view to render on screen
    return convertView;
}
}