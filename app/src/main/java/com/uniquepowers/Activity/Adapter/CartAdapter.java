package com.uniquepowers.Activity.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.uniquepowers.Activity.Model.CartModel;
import com.uniquepowers.Activity.Model.OrderModel;
import com.uniquepowers.R;

import java.util.ArrayList;

/**
 * Created by user on 19-Jul-17.
 */

public class CartAdapter extends ArrayAdapter<CartModel> implements View.OnClickListener {
    private ArrayList<CartModel> dataSet;
    Context mContext;
    int i=1;

    // View lookup cache
    private static class ViewHolder {
        TextView txtName;
        TextView quantity;
        TextView price;
        TextView show;
        TextView id;
        TextView date;
        TextView txtVersion;
        ImageView info;
        ImageView plus;
        ImageView sub1;
    }

    public CartAdapter(ArrayList<CartModel> data, Context context) {
        super(context, R.layout.cart_item, data);
        this.dataSet = data;
        this.mContext = context;

    }

    @Override
    public void onClick(View v) {

        int position = (Integer) v.getTag();
        Object object = getItem(position);
        CartModel newsModel = (CartModel) object;
//
//        switch (v.getId()) {
//            case R.id.plus:
//
//            int total=i+1;
//                Toast.makeText(getContext(),"ac"+total,Toast.LENGTH_LONG).show();
//                Toast.makeText(getContext(),"hrh"+i,Toast.LENGTH_LONG).show();
//
//            break;
//        }
    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
       CartModel newsModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
       CartAdapter.ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new CartAdapter.ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.cart_item, parent, false);
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.name);

           viewHolder.quantity = (TextView) convertView.findViewById(R.id.quantity);
            viewHolder.price = (TextView) convertView.findViewById(R.id.price);
            viewHolder.plus = (ImageView) convertView.findViewById(R.id.plus1);
            viewHolder.sub1 = (ImageView) convertView.findViewById(R.id.sub1);
          final TextView show = (TextView) convertView.findViewById(R.id.show);
viewHolder.plus.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        String present_value_string =show .getText().toString();
        int present_value_int = Integer.parseInt(present_value_string);
        if (present_value_int !=10){
            present_value_int++;
            show.setText(String.valueOf(present_value_int));

        }
    }
});



viewHolder.sub1.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        String present_value_string = show.getText().toString();
        int present_value_int = Integer.parseInt(present_value_string);
        if (present_value_int!=0) {
            present_value_int--;
            show.setText(String.valueOf(present_value_int));
        }

    }
});
            result = convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (CartAdapter.ViewHolder) convertView.getTag();
            result = convertView;
        }

//        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
//        result.startAnimation(animation);
        lastPosition = position;
        viewHolder.txtName.setText(newsModel.getName());
        viewHolder.quantity.setText("Availability: "+newsModel.getQuantity());
        viewHolder.price.setText("Price :  $ "+ newsModel.getPrice());

//
//        Picasso.with(getContext())
//                .load("http://agighana.org/uploaded_files/menu/"+newsModel.getFeature())
//                .into(viewHolder.info);

//        viewHolder.txtVersion.setText(newsModel.getVersion_number());
//        viewHolder.info.setOnClickListener(this);
//        viewHolder.info.setTag(position);
        // Return the completed view to render on screen
        return convertView;
    }
}