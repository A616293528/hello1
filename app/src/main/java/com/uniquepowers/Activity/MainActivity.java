package com.uniquepowers.Activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.uniquepowers.Activity.Fragment.Cart;
import com.uniquepowers.Activity.Fragment.Categories;
import com.uniquepowers.Activity.Fragment.Home;
import com.uniquepowers.Activity.Fragment.Login;
import com.uniquepowers.Activity.Fragment.MyOrder;
import com.uniquepowers.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @InjectView(R.id.cartButtonIV)
    ImageView cart;
    @InjectView(R.id.textNotify)
            TextView  text;
    int itemCounter=0;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
            ButterKnife.inject(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getFragmentManager().beginTransaction().add(R.id.frame_dashboard, new Home()).commit();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

            // cart button on click the perform the acton
            cart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getFragmentManager().beginTransaction().replace(R.id.frame_dashboard,new Cart()).addToBackStack(null).commit();
                    Toast.makeText(getApplicationContext(),"Show the Details in Cart",Toast.LENGTH_LONG).show();
                }
            });


  }
    public void addItemToCart() {
        TextView text = (TextView)findViewById(R.id.textNotify);
        text.setText(String.valueOf(++itemCounter));
    }
        @Override
        public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
/*
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.add_products) {
            getFragmentManager().beginTransaction().replace(R.id.frame_dashboard,new Cart()).addToBackStack(null).commit();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.home) {
            getFragmentManager().beginTransaction().add(R.id.frame_dashboard, new Home()).addToBackStack(null).commit();

            // Handle the camera action
        } else if (id == R.id.sleeves) {
            getFragmentManager().beginTransaction().add(R.id.frame_dashboard, new MyOrder()).addToBackStack(null).commit();

         } else if (id == R.id.tights) {


         } else if (id == R.id.hoodies) {


        } else if (id == R.id.platinumclothes) {

        }

        else if (id == R.id.sleeveless) {
            getFragmentManager().beginTransaction().add(R.id.frame_dashboard, new Categories()).addToBackStack(null).commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        //Cart button in Click to perform the action


        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
