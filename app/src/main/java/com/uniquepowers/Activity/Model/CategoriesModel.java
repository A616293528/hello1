package com.uniquepowers.Activity.Model;

/**
 * Created by Ashish on 7/3/2017.
 */

public class CategoriesModel {

    String name;
    String type;
    String news_id;
    String feature;

    public CategoriesModel(String name) {
        this.name=name;
        this.type=type;
        this.feature=feature;
        this.news_id=news_id;

    }

    public String getName() {
        return name;
    }
    public String getType() {
        return type;
    }

    public String getFeature() {
        return feature;
    }

    public String getNews_id() {
        return news_id;
    }

    public void setName(String name) {
        this.name = name;
    }


    public void setNews_id(String id) {
        this.news_id = id;
    }

    public void setType(String type) {
        this.type = type;
    }



    public void setFeature(String feature) {
        this.feature = feature;
    }
}
