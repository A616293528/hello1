package com.uniquepowers.Activity.Model;

/**
 * Created by user on 17-Jul-17.
 */

public class SubcategoryModel {

    String name;
    String type;
 int price;
    String feature;

    public SubcategoryModel(String image,String name,int price) {
        this.name=name;
        this.type=image;
        this.feature=feature;
        this.price=price;
     }

    public String getName() {
        return name;
    }
    public String getType() {
        return type;
    }

    public String getFeature() {
        return feature;
    }
    public void setName(String name) {
        this.name = name;
    }


    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setType(String type) {
        this.type = type;
    }



    public void setFeature(String feature) {
        this.feature = feature;
    }

}

