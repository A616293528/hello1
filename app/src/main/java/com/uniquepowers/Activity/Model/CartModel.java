package com.uniquepowers.Activity.Model;

/**
 * Created by user on 19-Jul-17.
 */

public class CartModel {
    String name;
    String quantity;
    String price;
    String feature;

    public CartModel(String name,String quantity,String price) {
        this.name=name;
        this.quantity=quantity;
        this.feature=feature;
        this.price=price;

    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }



    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }
}
