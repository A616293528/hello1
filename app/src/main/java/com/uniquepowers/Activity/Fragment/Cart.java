package com.uniquepowers.Activity.Fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.uniquepowers.Activity.Adapter.CartAdapter;
import com.uniquepowers.Activity.Model.CartModel;
import com.uniquepowers.Activity.PaymenttActivity;
import com.uniquepowers.R;

import java.util.ArrayList;

import butterknife.ButterKnife;

/**
 * Created by user on 18-Jul-17.
 */

public class Cart extends Fragment {
//        @InjectView(R.id.brand)
//        TextView brand;
//    @InjectView(R.id.avail)
//    TextView avail1;
//    @InjectView(R.id.price)
//    TextView price1;
    ListView listView;
    ListView list;
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.cart, parentViewGroup, false);
        ButterKnife.inject(this, rootView);
        Bundle bundle=this.getArguments();
             if (bundle != null) {
             String  brandname = bundle.getString("brand", "");
             String  avail = bundle.getString("avail", "");
             String  price = bundle.getString("price", "");
//            brand.setText(brandname);
//            avail1.setText(avail);
//            price1.setText(price);
             Toast.makeText(getActivity(),brandname+avail+price,Toast.LENGTH_LONG).show();
         }
         list=(ListView)rootView.findViewById(R.id.list);
         final ArrayList categoryModels = new ArrayList<CartModel>();
         categoryModels.add(new CartModel("Full Sleeve","In Stock","100"));
         CartAdapter adapter= new CartAdapter(categoryModels,getActivity());
         list.setAdapter(adapter);
         list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                CartModel newsModel = (CartModel) categoryModels.get(i);
                Intent ii=new Intent(getActivity(),PaymenttActivity.class);
                ii.putExtra("price",newsModel.getPrice());
                getActivity().startActivity(ii);
            }
        });
       return rootView;
    }
}
