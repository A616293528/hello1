package com.uniquepowers.Activity.Fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.uniquepowers.Activity.Adapter.OrderAdapter;
import com.uniquepowers.Activity.Model.OrderModel;
import com.uniquepowers.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by user on 13-Jul-17.
 */

public class MyOrder extends Fragment {

    HashMap<String, List<String>> listDataChild;
    ListView listView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.myorder, parentViewGroup, false);
        ButterKnife.inject(this, rootView);
        ListView list=(ListView)rootView.findViewById(R.id.lv);
           ArrayList categoryModels = new ArrayList<OrderModel>();
        categoryModels.add(new OrderModel("Full Sleeve"));
        categoryModels.add(new OrderModel("Platinum Clothes"));
        categoryModels.add(new  OrderModel("Sleeeveless"));
        categoryModels.add(new OrderModel("Training"));
        categoryModels.add(new  OrderModel("UniqueFit"));
        OrderAdapter  adapter= new OrderAdapter(categoryModels,getActivity());
        list.setAdapter(adapter);
        return  rootView;

    }
}
