package com.uniquepowers.Activity.Fragment;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.uniquepowers.Activity.Adapter.ListViewAdapter;

import com.uniquepowers.Activity.Model.SubcategoryModel;
import com.uniquepowers.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by user on 17-Jul-17.
 */
         public class SubCategory extends Fragment {
         ArrayList<SubcategoryModel> pollsModels = new ArrayList<>();;
         GridView gridView;
         @InjectView(R.id.simpleEditText)
         AutoCompleteTextView edittext;
         private static ListViewAdapter adapter;
         ArrayList<SubcategoryModel> countryList = new ArrayList<SubcategoryModel>();
         @InjectView(R.id.sort)
         ImageView sort;
    RadioGroup rg1;
    RadioButton img1,img2,img3,img4,img5,img6;
         @Override
         public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
             final View rootView = inflater.inflate(R.layout.subcategory, parentViewGroup, false);
             ButterKnife.inject(this, rootView);
             gridView = (GridView) rootView.findViewById(R.id.gridView);
             gridView.setTextFilterEnabled(true);
            // SubcategoryModel country = new SubcategoryModel("", "Afghanistan");
             countryList.add(new SubcategoryModel("", "Jan",10));
             countryList.add(new SubcategoryModel("", "Feb",20));
             countryList.add(new SubcategoryModel("", "March",35));
             countryList.add(new SubcategoryModel("", "April",100));
             countryList.add(new SubcategoryModel("", "May",5));
             adapter = new ListViewAdapter(getActivity(), countryList);
             gridView.setAdapter(adapter);
             sort.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 final BottomSheetDialog dialog = new BottomSheetDialog(getActivity());
                 getActivity().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                 dialog.setContentView(R.layout.bootom_dialog);
                 ImageView close=(ImageView)dialog.findViewById(R.id.close);
                 close.setOnClickListener(new View.OnClickListener() {
                     @Override
                     public void onClick(View view) {
                         dialog.dismiss();
                     }
                 });
                 TextView a=(TextView)dialog.findViewById(R.id.aa);
                 a.setOnClickListener(new View.OnClickListener() {
                     @Override
                     public void onClick(View view) {
                         Collections.sort(countryList, new SortArrayListAlphabetically());
                         for (SubcategoryModel emp : countryList) {
                             adapter = new ListViewAdapter(getActivity(), countryList);
                             gridView.setAdapter(adapter);
                             dialog.dismiss();

                         }

                     }
                 });
TextView z=(TextView)dialog.findViewById(R.id.nearest);
                 z.setOnClickListener(new View.OnClickListener() {
                     @Override
                     public void onClick(View view) {
                         Collections.sort(countryList, new SortArrayListAlphabetically1());
                         for (SubcategoryModel emp : countryList) {
                             adapter = new ListViewAdapter(getActivity(), countryList);
                             gridView.setAdapter(adapter);
                             dialog.dismiss();

                         }
                     }
                 });
                TextView low=(TextView)dialog.findViewById(R.id.low);
                 low.setOnClickListener(new View.OnClickListener() {
                     @Override
                     public void onClick(View view) {
                         Collections.sort(countryList, new SortArrayListPrice());
                         for (SubcategoryModel emp : countryList) {
                             adapter = new ListViewAdapter(getActivity(), countryList);
                             gridView.setAdapter(adapter);
                             dialog.dismiss();

                         }
                     }
                 });

                 TextView high=(TextView)dialog.findViewById(R.id.high);
                 high.setOnClickListener(new View.OnClickListener() {
                     @Override
                     public void onClick(View view) {

                         Collections.sort(countryList, new SortArrayListPricee());
                         for (SubcategoryModel emp : countryList) {
                             adapter = new ListViewAdapter(getActivity(), countryList);
                             gridView.setAdapter(adapter);
                             dialog.dismiss();

                         }

                     }
                 });
                 dialog.show();


    }
});
               gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                     @Override
                     public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                         ProductDescription fr = new ProductDescription();
                         android.app.FragmentManager fm = getActivity().getFragmentManager();
                         FragmentTransaction ft = fm.beginTransaction();
                         ft.add(R.id.frame_dashboard, fr);
                         ft.addToBackStack(null);
                         ft.commit();
                     }
                 });
                 edittext.addTextChangedListener(new TextWatcher() {
                     @Override
                     public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                     }

                     @Override
                     public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                         // TODO Auto-generated method stub
                         String text = edittext.getText().toString().toLowerCase(Locale.getDefault());
                         adapter.filter(text);
                     }

                     @Override
                     public void afterTextChanged(Editable editable) {
                     }
                 });
             return rootView;
             }
    class SortArrayListAlphabetically implements Comparator<SubcategoryModel> {
        @Override
        public int compare(SubcategoryModel subcategoryModel, SubcategoryModel t1) {
            return subcategoryModel.getName().compareToIgnoreCase(t1.getName());
        }
    }


    class SortArrayListAlphabetically1 implements Comparator<SubcategoryModel> {
        @Override
        public int compare(SubcategoryModel subcategoryModel, SubcategoryModel t1) {
            return t1.getName().compareToIgnoreCase(subcategoryModel.getName());
        }
    }


    public class SortArrayListPrice implements Comparator<SubcategoryModel> {
        @Override
        public int compare(SubcategoryModel e1, SubcategoryModel e2) {

            if (e1.getPrice() > e2.getPrice()) {
                return 1;
            } else if (e1.getPrice() == e2.getPrice()) {
                return 0;
            } else {
                return -1;
            }
        }
    }


    public class SortArrayListPricee implements Comparator<SubcategoryModel> {
        @Override
        public int compare(SubcategoryModel e1, SubcategoryModel e2) {

            if (e2.getPrice() > e1
                    .getPrice()) {
                return 1;
            } else if (e2.getPrice() == e1.getPrice()) {
                return 0;
            } else {
                return -1;
            }
        }


    }





    @Override
    public void onResume() {
        super.onResume();
    }
    @Override
    public void onPause() {
        super.onPause();
    }
}

