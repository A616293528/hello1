package com.uniquepowers.Activity.Fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.uniquepowers.R;

import butterknife.ButterKnife;

public class SingleItemView extends Activity {
	// Declare Variables
	TextView txtrank;
	TextView txtcountry;
	TextView txtpopulation;
	String rank;
	String country;
	String population;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.singleitemview);
		// Retrieve data from MainActivity on item click event
		Intent i = getIntent();
		// Get the results of rank
		rank = i.getStringExtra("rank");
		// Get the results of country
		country = i.getStringExtra("country");
		// Get the results of population
		population = i.getStringExtra("population");

		// Locate the TextViews in singleitemview.xml
		txtrank = (TextView) findViewById(R.id.rank);
		txtcountry = (TextView) findViewById(R.id.country);
		txtpopulation = (TextView) findViewById(R.id.population);

		// Load the results into the TextViews
		txtrank.setText(rank);
		txtcountry.setText(country);
		txtpopulation.setText(population);
	}
//
//	@Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
//		View rootView = inflater.inflate(R.layout.singleitemview, parentViewGroup, false);
//		ButterKnife.inject(this, rootView);
//
//
//		Intent i =getActivity().getIntent();
//		// Get the results of rank
//		rank = i.getStringExtra("rank");
//		// Get the results of country
//		country = i.getStringExtra("country");
//		// Get the results of population
//		population = i.getStringExtra("population");
//
//		// Locate the TextViews in singleitemview.xml
//		txtrank = (TextView)rootView. findViewById(R.id.rank);
////		txtcountry = (TextView)rootView. findViewById(R.id.country);
////		txtpopulation = (TextView)rootView. findViewById(R.id.population);
//
//		// Load the results into the TextViews
//		txtrank.setText(rank);
//		txtcountry.setText(country);
//		txtpopulation.setText(population);
//
//
//		return rootView;
//	}
}