package com.uniquepowers.Activity.Fragment;

import android.animation.Animator;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.uniquepowers.Activity.MainActivity;
import com.uniquepowers.Activity.PaymenttActivity;
import com.uniquepowers.Activity.Util.CircleAnimationUtil;
import com.uniquepowers.R;

import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;


        public class ProductDescription extends Fragment implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {
        @InjectView(R.id.spinner)
        Spinner spinner;
        @InjectView(R.id.sliderDetail)
        SliderLayout mDemoSlider;
        @InjectView(R.id.addtocart)
        Button cart;
        @InjectView(R.id.brandname)
        TextView brandname;
        @InjectView(R.id.availability)
        TextView availability;
        @InjectView(R.id.price)
        TextView price;
        @InjectView(R.id.description)
        TextView descripttion;
        @InjectView(R.id.features)
        TextView features;
        @InjectView(R.id.quantity)
        TextView quantity;
        @InjectView(R.id.buynow)
        Button buynow;
        String[] country = {"1", "2", "3", "4", "5",};
        private int itemCounter = 0;
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.productdescription, parentViewGroup, false);
        ButterKnife.inject(this, rootView);
        HashMap<String, Integer> file_maps = new HashMap<String, Integer>();
        file_maps.put("Hoodies ", R.drawable.image);
        file_maps.put("Hoodies ", R.drawable.image);
        file_maps.put("Hoodies ", R.drawable.image);
        file_maps.put("Hoodies ", R.drawable.image);
        for (String name : file_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(getActivity());
            // initialize a SliderLatiyout
                     textSliderView
                    .description(name)
                    .image(file_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);
            mDemoSlider.addSlider(textSliderView);
            ArrayAdapter aa = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, country);
            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            //Setting the ArrayAdapter data on the Spinner
            spinner.setAdapter(aa);
        }
                  cart.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {

                          
                            ((MainActivity)getActivity()).addItemToCart();



                      }

        });

         buynow.setOnClickListener(new View.OnClickListener() {
         @Override
        public void onClick(View view) {
        Intent ii=new Intent(getActivity(), PaymenttActivity.class);
        ii.putExtra("price","100");
        getActivity().startActivity(ii);
        }

    }
);
        return rootView;
    }
    @Override
    public void onSliderClick(BaseSliderView slider) {
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }



}
