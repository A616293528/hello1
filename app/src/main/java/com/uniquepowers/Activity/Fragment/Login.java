package com.uniquepowers.Activity.Fragment;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.uniquepowers.Activity.MainActivity;
import com.uniquepowers.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by user on 12-Jul-17.
 */

public class Login extends Fragment {
@InjectView(R.id.btn_skip)
    TextView skip;
    @InjectView(R.id.btn_login)
    Button login;
    @InjectView(R.id.signup)
    TextView signup;
    @InjectView(R.id.email)
    EditText emaill;
    @InjectView(R.id.password)
            EditText password;
    String url;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.login, parentViewGroup, false);
        ButterKnife.inject(this, rootView);
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ii=new Intent(getActivity(), MainActivity.class);
                startActivity(ii);
            }
        });

      login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ii=new Intent(getActivity(), MainActivity.class);
                startActivity(ii);


            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction().add(R.id.login_main,new SignUp()).addToBackStack(null).commit();
            }
        });
        return  rootView;
    }
    private void registerUser() {
        //Store data in Shared prefrance


       final String email=emaill.getText().toString();
        final String pass=password.getText().toString();



        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        //  Toast.makeText(getActivity(),response,Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }


                }) {
               @Override
               protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email",email);
                params.put("password",pass);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }


}
