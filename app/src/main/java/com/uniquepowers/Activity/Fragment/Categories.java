package com.uniquepowers.Activity.Fragment;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.uniquepowers.Activity.Adapter.ExpandableListAdapter;
import com.uniquepowers.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;

        public class Categories extends Fragment {
        ExpandableListAdapter listAdapter;
        ExpandableListView expListView;
        List<String> listDataHeader;
        HashMap<String, List<String>> listDataChild;
        @Override
         public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
         View rootView = inflater.inflate(R.layout.categories, parentViewGroup, false);
         ButterKnife.inject(this, rootView);
        expListView = (ExpandableListView)rootView.findViewById(R.id.lvExp);
        prepareListData();
        listAdapter = new ExpandableListAdapter(getActivity(), listDataHeader, listDataChild);
        // setting list adapter
        expListView.setAdapter(listAdapter);
        // Listview Group click listener
        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                 Toast.makeText(getActivity(),
                 "Group Clicked " + listDataHeader.get(groupPosition),
                 Toast.LENGTH_SHORT).show();
                  return false;
            }
        });

        // Listview Group expanded listener
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                Toast.makeText(getActivity(),
                        listDataHeader.get(groupPosition) + " Expanded",
                        Toast.LENGTH_SHORT).show();
            }
        });

        // Listview Group collasped listener
        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                Toast.makeText(getActivity(),
                        listDataHeader.get(groupPosition) + " Collapsed",
                        Toast.LENGTH_SHORT).show();

            }
        });

        // Listview on child click listener
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub
                Toast.makeText(
                     getActivity(),
                        listDataHeader.get(groupPosition)
                                + " : "
                                + listDataChild.get(
                                listDataHeader.get(groupPosition)).get(
                                childPosition), Toast.LENGTH_SHORT)
                        .show();
                SubCategory fr = new SubCategory();
                android.app.FragmentManager fm = getActivity().getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                Bundle args = new Bundle();
                ft.add(R.id.frame_dashboard, fr);
                ft.addToBackStack(null);
                ft.commit();

                return false;
            }
        });


//        listView=(ListView)rootView.findViewById(R.id.list);
//        categoryModels = new ArrayList<>();
//        categoryModels.add(new CategoriesModel("Sleeves"));
//        categoryModels.add(new CategoriesModel("Hoodies"));
//        categoryModels.add(new CategoriesModel("Sleeeveless"));
//        categoryModels.add(new CategoriesModel("Training"));
//        categoryModels.add(new CategoriesModel("UniqueFit"));
//        adapter= new CategoriesAdapter(categoryModels,getActivity());
//        listView.setAdapter(adapter);
        return rootView;

    }
         private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();
        // Adding child data
        listDataHeader.add("Sleeves");
        listDataHeader.add("Tights");
        listDataHeader.add("Hoodies");
        listDataHeader.add("Platinum Clothes");
        listDataHeader.add("Unique Fit");
        listDataHeader.add("Training");
        listDataHeader.add("Sleeveless");
        // Adding child data
        List<String> top250 = new ArrayList<String>();
        top250.add("Long Sleeves");
        top250.add("Short Sleeves");
//        top250.add("The Godfather: Part II");
//        top250.add("Pulp Fiction");
//        top250.add("The Good, the Bad and the Ugly");
//        top250.add("The Dark Knight");
//        top250.add("12 Angry Men");
        List<String> nowShowing = new ArrayList<String>();
        nowShowing.add("Leggings Tights");
        nowShowing.add("Short Tights");
//        nowShowing.add("Turbo");
//        nowShowing.add("Grown Ups 2");
//        nowShowing.add("Red 2");
//        nowShowing.add("The Wolverine");
        List<String> comingSoon = new ArrayList<String>();
       /* comingSoon.add("2 Guns");
        comingSoon.add("The Smurfs 2");
        comingSoon.add("The Spectacular Now");
        comingSoon.add("The Canyons");
        comingSoon.add("Europa Report");
*/
        List<String> coming1 = new ArrayList<String>();
        List<String> coming2 = new ArrayList<String>();
        List<String> coming3 = new ArrayList<String>();
        List<String> coming4 = new ArrayList<String>();
        listDataChild.put(listDataHeader.get(0), top250); // Header, Child data
        listDataChild.put(listDataHeader.get(1), nowShowing);
        listDataChild.put(listDataHeader.get(2), comingSoon);
        listDataChild.put(listDataHeader.get(3), coming1);
        listDataChild.put(listDataHeader.get(4), coming2);
        listDataChild.put(listDataHeader.get(5), coming3);
        listDataChild.put(listDataHeader.get(6), coming4);
    }
}
