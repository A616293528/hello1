package com.uniquepowers.Activity.Fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.uniquepowers.Activity.MainActivity;
import com.uniquepowers.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by user on 12-Jul-17.
 */

public class SignUp extends Fragment {
    @InjectView(R.id.llSignupLogin)
    LinearLayout loginsignup;
    @InjectView(R.id.btnSignupSignup)
    Button signup;
         @InjectView(R.id.spinner)
       Spinner spinner;
         @InjectView(R.id.spinner1)
           Spinner spinner1;
           @InjectView(R.id.f_name)
            EditText fname;
            @InjectView(R.id.l_name)
            EditText lname;
            @InjectView(R.id.email)
            EditText email;
            @InjectView(R.id.mobile)
            EditText mobile;
           @InjectView(R.id.fax)
            EditText fax;
           @InjectView(R.id.company)
            EditText company;
           @InjectView(R.id.address)
            EditText address;
           @InjectView(R.id.addres2)
            EditText address2;
            @InjectView(R.id.city)
            EditText city;
           @InjectView(R.id.postcode)
            EditText postcode;
           @InjectView(R.id.password) EditText password;
           @InjectView(R.id.c_password)
            EditText c_password;

    String[] country = { "Select","India", "USA", "China", "Japan", "Other",  };
    String url;
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.signup, parentViewGroup, false);
        ButterKnife.inject(this, rootView);
        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.my_awesome_toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_home_as_up));
        ArrayAdapter aa = new ArrayAdapter(getActivity(),android.R.layout.simple_spinner_item,country);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        spinner.setAdapter(aa);
        spinner1.setAdapter(aa);

       signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ii=new Intent(getActivity(), MainActivity.class);
                startActivity(ii);
            }
        });
loginsignup.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        getFragmentManager().popBackStack();
    }
});
        return rootView;
    }


    private void registerUser() {
        final String f_name=fname.getText().toString();
        final String l_name=lname.getText().toString();
        final String emailid=email.getText().toString();
        final String mobileno=mobile.getText().toString();
        final String faxno=fax.getText().toString();
        final String companyy=company.getText().toString();
        final String address11=address.getText().toString();
        final String address22=address2.getText().toString();
        final String cityy=city.getText().toString();
        final String postcodee=postcode.getText().toString();
        final String paswordd=password.getText().toString();
        final String cpassword=c_password.getText().toString();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        //  Toast.makeText(getActivity(),response,Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }


                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("f_name", f_name);
                params.put("l_name", l_name);
                params.put("email", emailid);
                params.put("mobile", mobileno);
                params.put("fax", faxno);
                params.put("company", companyy);
                params.put("address", address11);
                params.put("addres1", address22);
                params.put("cityy", cityy);
                params.put("postcodee", postcodee);
                params.put("paswordd", paswordd);
                params.put("cpassword", cpassword);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }


    @Override
    public void onResume() {
        super.onResume();

    }
    @Override
    public void onPause() {
        super.onPause();
    }
    @Override
    public void onStop() {
        super.onStop();

    }

}
